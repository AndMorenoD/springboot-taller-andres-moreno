package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.WebServiceEntity;

public interface IWebServiceInterface {
	
		//Para Listar WebServiceEntity
		public List<WebServiceEntity> findAll();
		
		//Para Editar WebServiceEntity
		public WebServiceEntity findById(Long id);
		
		//Para Crear WebServiceEntity
		public WebServiceEntity save(WebServiceEntity service);
		
		//Para Paginar
		public Page<WebServiceEntity> findAll(Pageable pageable);


}
