package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.DAO;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.OperationEntity;

public interface IOperationServiceDAO extends PagingAndSortingRepository<OperationEntity, Long > {

}
