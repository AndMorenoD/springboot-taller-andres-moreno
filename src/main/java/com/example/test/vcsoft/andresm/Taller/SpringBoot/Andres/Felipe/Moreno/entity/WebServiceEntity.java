package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "web_service_entity")
public class WebServiceEntity implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//Nombre de la API
	private String apiName;
	
	//Nombre del tecnico
	private String expertName;
	
	//Numero de la version
	private String version;
	
	//Objetivo de negocio
	private String dealObject;
	
	//Dueño funcional
	@JoinColumn(name = "id_owner_func")
	@ManyToOne(cascade = CascadeType.ALL)
	private OwnerFunc ownerFuncion;
	
	//Dueño tecnico
	@JoinColumn(name = "id_expert_owner")
	@ManyToOne(cascade = CascadeType.ALL)
	private ExpertOwner expertOwner;
	
	@ManyToMany(cascade= CascadeType.ALL)
	@JoinTable(
			  name = "interfaces_operation", 
			  joinColumns = @JoinColumn(name = "id"), 
			  inverseJoinColumns = @JoinColumn(name = "id_operation"))
	private List<OperationEntity> interfaceService;
	
	
	
	public ExpertOwner getExpertOwner() {
		return expertOwner;
	}

	public void setExpertOwner(ExpertOwner expertOwner) {
		this.expertOwner = expertOwner;
	}

	public List<OperationEntity> getInterfaceService() {
		return interfaceService;
	}

	public void setInterfaceService(List<OperationEntity> interfaceService) {
		this.interfaceService = interfaceService;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getExpertName() {
		return expertName;
	}

	public void setExpertName(String expertName) {
		this.expertName = expertName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDealObject() {
		return dealObject;
	}

	public void setDealObject(String dealObject) {
		this.dealObject = dealObject;
	}

	public OwnerFunc getOwnerFuncion() {
		return ownerFuncion;
	}

	public void setOwnerFuncion(OwnerFunc ownerFuncion) {
		this.ownerFuncion = ownerFuncion;
	}
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

}
