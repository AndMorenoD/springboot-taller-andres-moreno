package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "owner_func")
public class OwnerFunc implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_owner_func;

	private String name;

	@Email
	private String email;

	// Constructor libre
	public OwnerFunc() {
	}

	// Constructor usando Fields
	public OwnerFunc(Long id, String name, @Email String email) {
		super();
		this.id_owner_func = id;
		this.name = name;
		this.email = email;
	}

	public Long getId() {
		return id_owner_func;
	}

	public void setId(Long id) {
		this.id_owner_func = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
