package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.DAO.IWebServiceDAO;
import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.WebServiceEntity;

@Service
public class WebServiceImp implements IWebServiceInterface {
	
	@Autowired
	private IWebServiceDAO serviceDAO;

	@Override
	public List<WebServiceEntity> findAll() {
		
		return (List<WebServiceEntity>) serviceDAO.findAll();
	}

	@Override
	public WebServiceEntity findById(Long id) {
		
		return serviceDAO.findById(id).orElse(null);
	}

	@Override
	public WebServiceEntity save(WebServiceEntity service) {
		
		return serviceDAO.save(service);
	}

	@Override
	public Page<WebServiceEntity> findAll(Pageable pageable) {
		
		return serviceDAO.findAll(pageable);
	}


}
