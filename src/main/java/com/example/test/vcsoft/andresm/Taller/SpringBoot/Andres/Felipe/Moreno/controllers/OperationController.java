package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.OperationEntity;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service.IOperationServiceInterface;


@RestController
@RequestMapping("/api")
public class OperationController {
	
	@Autowired
	private IOperationServiceInterface service;
	
	@GetMapping("/listoperation")
	@ResponseStatus(code = HttpStatus.OK)
	public List<OperationEntity> list()
	{
		return service.findAll();
	}
	
	@PostMapping("/operation")
	@ResponseStatus(code = HttpStatus.CREATED)
	public OperationEntity save(@RequestBody OperationEntity operation) 
	{
		return service.save(operation);
	}

}
