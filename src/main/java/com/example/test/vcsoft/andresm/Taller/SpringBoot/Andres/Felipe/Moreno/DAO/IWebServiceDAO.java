package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.DAO;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.WebServiceEntity;

public interface IWebServiceDAO extends PagingAndSortingRepository<WebServiceEntity, Long> {

}
