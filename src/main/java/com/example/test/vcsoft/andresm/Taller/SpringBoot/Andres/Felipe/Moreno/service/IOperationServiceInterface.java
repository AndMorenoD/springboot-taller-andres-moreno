package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service;

import java.util.List;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.OperationEntity;

public interface IOperationServiceInterface {

	// Para Listar OperationEntity
	public List<OperationEntity> findAll();

	// Para Editar OperationEntity
	public OperationEntity findById(Long id);

	// Para Crear OperationEntity
	public OperationEntity save(OperationEntity operation);


}
