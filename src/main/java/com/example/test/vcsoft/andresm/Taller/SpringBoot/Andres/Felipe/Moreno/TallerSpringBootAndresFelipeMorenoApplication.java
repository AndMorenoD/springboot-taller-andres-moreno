package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TallerSpringBootAndresFelipeMorenoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TallerSpringBootAndresFelipeMorenoApplication.class, args);
	}

}
