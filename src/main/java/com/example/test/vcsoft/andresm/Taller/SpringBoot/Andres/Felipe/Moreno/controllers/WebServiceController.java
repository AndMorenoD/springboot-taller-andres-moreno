package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.WebServiceEntity;
import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service.IWebServiceInterface;

@CrossOrigin(origins = {"*"})
@RestController
@RequestMapping("/api")
public class WebServiceController {
	
	@Autowired
	private IWebServiceInterface service;
	
	@GetMapping("/listwebservice")
	@ResponseStatus(code = HttpStatus.OK)
	public Page<WebServiceEntity> list(@PageableDefault(size = 10,page = 0) Pageable pageable)
	{
		
		Page<WebServiceEntity> result = service.findAll(pageable);
		
		return result ;
	}
	
	@PostMapping("/webservice")
	@ResponseStatus(code = HttpStatus.CREATED)
	public WebServiceEntity save(@RequestBody WebServiceEntity webService) 
	{
		return service.save(webService);
	}

}
