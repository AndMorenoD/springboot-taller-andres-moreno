package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.DAO.IOperationServiceDAO;
import com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity.OperationEntity;


@Service
public class OperationServiceImp implements IOperationServiceInterface {
	
	@Autowired
	private IOperationServiceDAO operationDAO;

	@Override
	public List<OperationEntity> findAll() {
		// TODO Auto-generated method stub
		return (List<OperationEntity>) operationDAO.findAll();
	}

	@Override
	public OperationEntity findById(Long id) {
		// TODO Auto-generated method stub
		return operationDAO.findById(id).orElse(null);
	}

	@Override
	public OperationEntity save(OperationEntity service) {
		// TODO Auto-generated method stub
		return operationDAO.save(service);
	}

}
