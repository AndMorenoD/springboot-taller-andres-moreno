package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "interface_entity")
public class OperationEntity implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_operation;

	private String name;
	private String method;
	private String path;
	private String request;
	private String response;
	
	public OperationEntity() {}

	public OperationEntity(Long id, String name, String method, String path, String request, String response) {
		super();
		this.id_operation = id;
		this.name = name;
		this.method = method;
		this.path = path;
		this.request = request;
		this.response = response;
	}

	public Long getId() {
		return id_operation;
	}

	public void setId(Long id) {
		this.id_operation = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
