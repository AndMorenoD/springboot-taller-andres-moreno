package com.example.test.vcsoft.andresm.Taller.SpringBoot.Andres.Felipe.Moreno.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "expert_owner")
public class ExpertOwner implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_expert_owner;

	private String name;

	
	private String email;
	
	public ExpertOwner() {}

	public ExpertOwner(Long id_expert_owner, String name, @Email String email) {
		super();
		this.id_expert_owner = id_expert_owner;
		this.name = name;
		this.email = email;
	}

	public Long getId_expert_owner() {
		return id_expert_owner;
	}

	public void setId_expert_owner(Long id_expert_owner) {
		this.id_expert_owner = id_expert_owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
